# ATOM is meeting on Tuesday, 19th February, 6:30pm, at Galvanize!

## Introduction
Our discussion this month will be led by Matthew Drury. We'll be discussing a paper, "To tune or not to tune the number of trees in random forest".

https://arxiv.org/pdf/1705.05654.pdf

Random Forest is one of the most generally useful machine learning models due to it's rough and tumble, hard to mess it up nature, and relatively good predictive performance. A common question when using a random forest is "how many trees", and more precisely "should I tune the number of trees using validation, or just go with the largest viable number".

This paper studies the issue in detail for a variety of performance metrics, and gives theoretical and empirical arguments that the correct answer is "don't tune, just bloom".

We'll talk through the following points:

- Review of random forests.
- Just what is a tuning parameter anyway?
- Are there multiple types of tuning parameters?
- What type of tuning parameter is # trees in a random forest.
- Discuss the arguments from the paper.
- Are there any weaknesses to the arguments in the paper.
- Do you agree with the conclusions?

Note: set aside some time for a bit of math on this one. The mathematics in this one is quite nice: the arguments are relatively straightforward and use only basic concepts from probability, but paper and pencil will help immensely.

## About ATOM:
Advanced Topics on Machine learning ( ATOM ) is a learning and discussion group for cutting-edge machine learning techniques in the real world. We work through winning Kaggle competition entries or real-world ML projects, learning from those who have successfully applied sophisticated data science pipelines to complex problems.

As a discussion group, we strongly encourage participation, so be sure to read up about the topic of conversation beforehand !

ATOM can be found on PuPPy’s Slack under the channel #atom, and on PuPPy’s Meetup.com events.   

We're kindly hosted by Galvanize (https://www.galvanize.com). Thank you !